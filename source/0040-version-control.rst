.. index::
    single: Version Control

================
Version Control
================

It's already common practice to use some sort version control tools.
However these are often built into the
proprietary technical authoring tools.

By using the same version control tools as our developers we gain
a number of benefits:

#.  It's easier for developers to work on technical documentation
#.  With the code and content sitting "next to" each people may feel greater
    sense of ownership if they can "see" the docs as part of the overall
    asset database
#.  The relationships between software and documentation releases is easier to track

The most popular version control tool today is `Git <https://git-scm.com>`_.
It has become the only tool discussed in the DAC "literature".

**However** it is important that you consider using the same version control tool
as the developers
and engineers on your product team. This will allow you to make use of their experience
and make it easier for them to contribute to your content.


One of the great debates of in the DAC community is about the location of the
documentation assets. Should they be stored in the same repository as the other
product artifacts (for example the source code) or should the docs
sit in a dedicated repository? The answer is that it depends on the
project requirements and the environment in which the project sits.
Expect to do both.

Considerations include:

* Does the product exist in multiple other projects?
    If the other product assets are spread across multiple source code
    repositories then it can be hard to build the documentation if it is
    in multiple locations.  
* If the product development team are **not** working on the docs
  then having a separate repository can make sense.
  NOTE: Using DAC approach in this environment can still make sense.


.. #TODO -- add more details
