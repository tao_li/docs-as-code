.. index::
    single: Developer Practices

=======================
Developer Practices
=======================

--------------------------
Adapt to Change & Feedback
--------------------------

We can improve both the content of documentation and the efficiency of how it is created.

Most teams already have ways to get feedback on their content.
The basic requirements for an agile change control process.


1. The need is captured into a request (this includes bug reports)
2. The request is triaged so that a definitive decision is made and a priority assigned
3. Work is completed
4. The work is verified using a combination of reviews and testing
5. Deployment occurs


NOTE:

    A developer style workflow will be useful for your Docs As Code process.
    **However** it may not be the same workflow used by your product engineers.
    Be careful to adapt processes and ways of working to the specific requirements
    of your technical writing projects.

--------------------------
Change Review Process
--------------------------

Using similar change control processed across both documentation and product
development artifacts gives a number of benefits

.. <!-- alex ignore joint -->

#.  Single process for everyone on the team
#.  Single set of tools
#.  Documentation and product changes can be discussed in the same
    forum and joint impacts understood
#.  ...

-----------------------------------
Exploit other Developer processes
-----------------------------------

* Use an agile story based approach to manage documentation design and purpose
  Using the same language as your development team to answer The who, what and how questions.

.. #TODO -- needs a lot more explanation.