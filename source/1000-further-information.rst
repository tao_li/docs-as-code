===================
Further information
===================

------
Books:
------

*   `Docs Like Code Book <https://www.amazon.com.au/dp/1387081322/ref=cm_sw_em_r_mt_dp_U_2JCHDbVQVXK6F>`_ by Anne Gentle
*   `Modern Technical Writing: An Introduction to Software Documentation <https://www.amazon.com.au/dp/B01A2QL9SS/ref=cm_sw_em_r_mt_dp_U_uFCHDb4CG90VR>`_ by Andrew Etter
*   `Quality Docs as Code: How to Maintain Your ISO 9001 Quality Management System Documentation with Plain Text <https://www.amazon.com.au/dp/B07V9SY4WL/ref=cm_sw_em_r_mt_dp_U_YyfLDbT309T2R>`_  by Michael Lynnmore

-------------
Free Content:
-------------

.. <!-- alex ignore UK -->

*   `Docs Like Code <https://www.docslikecode.com/>`_ website
*   Docs As Code at `Write The Docs <https://www.writethedocs.org/guide/docs-as-code/>`_
*   `Engineering Great Documentation <https://devrel.net/developer-experience/docops-engineering-great-documentation>`_ by Adam Butler
    at DevRelCon London 2017
*   Thoughts on `...docs-as-code after 3 years -- it works! <https://idratherbewriting.com/2018/07/03/docs-as-code-after-three-years/>`_ by Tom Johnson
*   `...Linters for the English Language <https://dzone.com/articles/lint-lint-and-away-linters-for-the-english-languag>`_ by Chris Ward
*   `Static Site Generators: What, Why, and How <https://noti.st/verythorough/qSKAbH/static-site-generators-what-why-and-how>`_ by Jessica Parsons
*   `An introduction to the Pandoc document formating tool <https://www.vala.org.au/events/vala-tech-camp-2019/camp-sessions-t8-clews/#>`_ By Alec Clews
*   `Why we use a ‘docs as code’ approach for technical documentation <https://technology.blog.gov.uk/2017/08/25/why-we-use-a-docs-as-code-approach-for-technical-documentation/>`_ by the UK Government
*   `The UK government meets docs as code <https://www.youtube.com/watch?v=Ql9Il7tssik>`_ by Jen Lambourne speaking at Write the Docs Prague 2019
*   `Who Writes the Docs? <https://www.youtube.com/watch?v=eOC6rsizIvM>`_ by Beth Aitman speaking at Write the Docs Portland 2018
*   `Docs-as-code: arc42, AsciiDoc, Gradle & Co. combined <https://www.youtube.com/watch?v=qr3NJzeKiCI>`_ by Ralf D. Muller at DR8Conf
*   `Treating Documentation like Code <https://www.youtube.com/watch?v=Mzu-c-FoOdw>`_ by Jodie Putrino at Write the Docs Portland 2017

and for a contrary view

*   `Limits to the idea of treating docs as code <https://idratherbewriting.com/2017/06/02/when-docs-are-not-like-code/>`_, also by Tom Johnson

-------------------
Online Communities:
-------------------

*   `"Docs as Code" channel <https://app.slack.com/client/T0299N2DL/C72NZ18FR>`_ on the Write the Docs Slack community

--------------------------
Related Tools and Projects
--------------------------

* `docToolChain <https://doctoolchain.github.io/docToolchain/>`_ is an implementation of the docs-as-code approach for software architecture based on the `arc42 <https://arc42.org/>`_ template
* `Plantuml <https://plantuml.com/>`_: Create various types of UML style diagrams for various purposes from the command line
* `Graphviz <https://www.graphviz.org/>`_: Create network diagrams from the command line
* `ImageMagick <https://imagemagick.org/>`_: Create, edit, compose, or convert images from the command line
* `Freeplane <https://www.freeplane.org/wiki/index.php/Home>`_: Create and edit mind maps, then export into an image file during the build (command line)
* `Pandoc <https://pandoc.org/>`_: The Swiss Army knife of document format converters.
