 
============================
Welcome to Docs-As-Code Demo
============================

Test 3

These notes were created to illustrate the talk
**Docs-As-Code: What, Why and How**,
first presented at the
`Australian Society for Technical Communication <https://www.astc.org.au/>`_
2019 Annual Conference and then at the `Linux Conf AU <https://linux.conf.au/>`_
2020 international conference.

.. raw:: html

    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Caveat:

    This online document has been created as an example for the docs-as-code presentation below,
    not a complete reference in its own right. It will have many shortcomings
    (which may get fixed over time).

The source content for this website can be found on `GitLab <https://gitlab.com/PaperCutSoftware/docs-as-code/>`_.

.. toctree::
  :maxdepth: 1
  :hidden:
  :glob:
   
  [0-9][0-9][0-9][0-9]-*  

-------------------
Indices and tables
-------------------

* :ref:`genindex`
* :ref:`search`

------------------------
Latest Version of Slides
------------------------

.. raw:: html

  <iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ8T10IT9JvcXCk1O73p4rIixGplQ-zXzAUAXNGvrebWg8bO6eZTBjuuiqvLAA71h-vS_EW40fQfARl/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
