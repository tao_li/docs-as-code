.. index::
    single: Introduction
    
============
Introduction
============

Docs As Code (also known as "Docs Like Code" and abbreviated here to DAC)
has become a popular topic in the technical writing community recently,
especially when writing documentation for software projects.

The phrase is open to many interpretations, but there are two common themes
that you can expect to see.

1. Adopt an ":term:`agile`" approach to content creation, namely
    #. The whole team is responsible for content
    #. Be adaptive and improve both your content, and your process, over time

2. Use developer tools, and process, to create and deliver content. Specifically:
    #. Text based file content with embedded, `lightweight`, markup tags
    #. Developer based workflows
        #. Version Control using tools such as Git
        #. Change control driven though bugs and feature requests tickets
        #. Content reviews and merges
    #. Machine generated content
    #. Automated testing and verification
    #. Rapid online publication using web pages (using static site generators) or wikis

Benefits Can Include

* Faster Publication Cycles
* Reduced load on the technical writing team to create content
* Reduced costs for monolithic authoring and publication tools


Docs-as-Code is still an evolving approach and you will see many variations
on these ideas. Tools and processes will need to be adapted
for your project (and then adapted again for the project after that).

Hopefully this material will provide a set of useful ideas, patterns, terminology,
tools and processes for your documentation toolkit.

.. marker1

----------------
Example Workflow
----------------

.. image:: images/workflow.png

.. marker2

--------
See also
--------
`...what we mean by docs like code <https://idratherbewriting.com/2017/06/02/when-docs-are-not-like-code/#first-what-we-mean-by-docs-like-code>`_
by Tom Johnson.
