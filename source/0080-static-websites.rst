.. index::
    single: Static Websites
    single: Online Publication

===================
Static Websites
===================

Many documentarians prefer to use static site generators to deploy
websites rather than using a content management system. For documentation
that many contain little or no dynamic content they have a number of
advantages

*   Simple infrastructure
*   Easy to use
*   Displays quickly in the user's browser


Examples of static site generators include

*   Jekyll
*   Sphinx
*   Hugo

See the :doc:`1000-further-information` section for an presentation about static site generators.
