.. index::
    single: Automation

==========
Automation
==========

Content creation for technical documentation needs a human to write and verify
the written words and diagrams.
However we can still get a lot of help by using tools to carry out as much work as possible.

Broadly we can use tools in three ways:

1. Content Creation
2. Verification
3. Publication and deployment

Furthermore we can also save a lot of tedious work if we can automatically run our tools using a CI/CD framework.

--------------------------
Automated Content Creation
--------------------------

It is possible to generate a wide range of content automatically. For example:

* Diagrams
* Reference material
* Table of contents, cross references etc.

As an example the image used in the section :doc:`0030-text-based-content`
is created at publication time by processing the following script with
`Graphviz <https://www.graphviz.org/>`_

.. literalinclude:: images/markdown2docbook.gv

Other example of text driven diagramming and image creation tools include

* `Plantuml <https://plantuml.com/>`_
* `ImageMagick <https://imagemagick.org/index.php>`_


Indexes, Table of Contents etc
===============================

These can be either part the publication platform, an add-in to the platform or stand alone programs
(e.g. `github-markdown-toc <https://github.com/ekalinin/github-markdown-toc>`_)

--------------------------
Verification
--------------------------

There are number of tools that can use used to check the quality of writing
and spot potential problems. For example

.. <!-- alex ignore dead -->

* `Lint Checkers <https://dzone.com/articles/lint-lint-and-away-linters-for-the-english-languag>`_,
  designed to highlight potential issues with your writing.
* Finding dead links. Google can help you find options, Sphinx provides a standard extension to check for
  dead links.
* Spell Checkers

Of course proof reading by a human is still required.

-----------------------------------
Publication
-----------------------------------

The markup text files used to edit the source content will generally not be suitable for deployment
to a publication platform (e.g. a static website). A translation process
will be required to create the final output. For instance this content
is published in both PDF and HTML.

-------------------------------------
Running Tools Using CI/CD Frameworks
-------------------------------------

:term:`CI/CD` provides a set of tools that will allow you to run
tools automatically on events and then deploy on a successful merge. Typical
events include a ''git push'', a branch merge, some external event e.g. an API call, etc.

As an example, the content for this website is processed using the GitLab CI/CD feature. The process works as follows

1. All automated testing and building is carried out using a custom :term:`Docker` image
2. Whenever work is pushed to any branch a CI/CD pipeline will carry out checks viz:
    a. Dead links
    b. Spelling
    c. Linting for potentially offensive language
3. When work is merged onto the ``master`` branch the same verification steps are performed and then the
   content is published to the website (using GitLab Pages)

Using a custom Docker image for this process provides a significant advantage.
The Docker image can be used locally by content creators to verify their changes locally, for

instance by running the command

.. code::

    ./runme.ps1

will run all three verification jobs. The sample ``runMe.ps1`` script will work in
PowerShell or WSL on Windows.

The command 

.. code::

   ./runme.ps1 make html

will create an html version for review at ``build/html/index.html``.

The content creator has not had to install any local tools (except for Docker, Git and a text
editor) to have a complete local development and test environment.

The added benefit is that both the production site and the local environment are identical.
For instance they both use the same locale settings so the spell checking works consistently.

The approach demonstrated here depends on a build script, in this case ``Makefle``.
