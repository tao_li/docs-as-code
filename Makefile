.SUFFIXES:
.PHONY: help tools spelling spell lint html all pdf images linkcheck verify todo alex write-good

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     := source
BUILDDIR      := build

MAKEFILE	  := ${lastword $(MAKEFILE_LIST)}

GENERATED_UML_IMAGES:=${patsubst $(SOURCEDIR)/images/%.plantuml,$(SOURCEDIR)/images/%.png,${wildcard $(SOURCEDIR)/images/*.plantuml}}

GENERATED_DOT_IMAGES:=${patsubst $(SOURCEDIR)/images/%.gv,$(SOURCEDIR)/images/%.png,${wildcard $(SOURCEDIR)/images/*.gv}}

all: verify html

pdf: latexpdf

%.png: %.gv
	dot -Tpng -o $@ $<

%.png: %.plantuml
	plantuml -tpng $<

images: $(GENERATED_UML_IMAGES) $(GENERATED_DOT_IMAGES)

$(SOURCEDIR)/example_content.md : $(SOURCEDIR)/0000-introduction.rst
	sed -ne '/marker1/,/marker2/p' $< | sed -e '1d;$$d'| pandoc -f rst -t gfm -o $@ -

$(SOURCEDIR)/example_content.dbx: $(SOURCEDIR)/example_content.md
	pandoc -t docbook -o $@ -f gfm $< 

include_files: $(SOURCEDIR)/example_content.dbx $(SOURCEDIR)/example_content.md

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
latexpdf html: $(MAKEFILE) images include_files
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

verify: spelling linkcheck lint

spell: spelling

gettext: include_files images $(MAKEFILE)
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

spelling linkcheck: gettext
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	test -s $(BUILDDIR)/$@/output.txt && cat $(BUILDDIR)/$@/output.txt && exit 1 || exit 0

tools: venv/bin/activate node_modules/.bin/alex

venv/bin/activate:
	-test -d venv && rm -rf venv
	python3 -m venv venv
	. venv/bin/activate && pip install -r requirements.txt

lint:
	find source -type f \( -name \*.rst -o -name \*.plantuml -o -name \*.gv \) -exec /node_modules/.bin/alex {} \; | \
	egrep '{\d}+:{\d}+-{\d}+:{\d}+\S+warning' && exit 1 || exit 0

# write-good needs more work to work under a CI/CD framework
write-good:
	-rm /tmp/$@.log
	find source -type f \( -name \*.rst -o -name \*.plantuml -o -name \*.gv \) -exec ./node_modules/.bin/write-good {} \; | \
	tee /tmp/$@.log | \
	egrep '^[[:space:]]*\^+[[:space:]]*$$' > /dev/null &&  cat /tmp/$@.log && exit 1 ||exit 0
	
todo:
	@find source -type f \( -name \*.rst -o -name \*.plantuml \) -exec grep -H '#TODO' {} \;

# NB Don't use $(IMAGES) here as it may contain images we can't regenrate
clean:
	-rm -rf $(BUILDDIR) $(SOURCEDIR)/example_content.* \
	$(SOURCEDIR)/images/workflow.png $(SOURCEDIR)/images/markdown2docbook.png 
