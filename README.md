These files support a talk that inroduces the concepts of Docs as Code.

Read the content at https://papercutsoftware.gitlab.io/docs-as-code

You should have docker installed to work on this project

To work on the content clone this repo and then pull the Docker image
`registry.gitlab.com/papercutsoftware/docs-as-code:latest`

By default the runDocTools script will drop you into a shell so you
play with the provided tools in the project environment.

The following commands are supported.

* `make html` - builds the doc site -- open the file `build/html/index.html`
* `make pdf` - builds a pdf version of the doc site -- open the file `build/latex/docslikecodedemo.pdf`
* `make spell` - perform a spellcheck (`spell` is an alias for `spelling`)
* `make linkcheck` - check links
* `make lint` - run lint checks
* `make verify` - runs all the above tests

Make targets may be combined e.g.

```sh
make spell lint html pdf
```
